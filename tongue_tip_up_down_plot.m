% get the names of the arrays
JW11_TP013.NAME

%name of the 6th array
JW11_TP013(6).NAME

%actual data (T2)
JW11_TP013(6).SIGNAL
n = size(JW11_TP013(6).SIGNAL)
time = 1:3078

T2_height = JW11_TP013(6).SIGNAL(:,2);
T1_height = JW11_TP013(5).SIGNAL(:,2);
d_height = T2_height-T1_height


subplot(2,1,1)
plot(time,T2_height) %plots T2 across the file
hold
plot(time,T1_height,'r') %plots T1 across the file

subplot(2,1,2)
plot(time,d_height,'k')

