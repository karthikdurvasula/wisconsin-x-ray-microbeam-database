
T2_height = JW11_TP013(6).SIGNAL(:,2);
T1_height = JW11_TP013(5).SIGNAL(:,2);
d_height = T2_height-T1_height;
max(d_height);
min(d_height);

% this is the timestamp of the /s/ to plot
timestamp = 9399.6;

t = round(timestamp/1000*JW11_TP013(5).SRATE);

% x and y coordinates for each sensor
%T1
T1_y_s = JW11_TP013(5).SIGNAL(t,2);
T1_x_s = JW11_TP013(5).SIGNAL(t,1);

T2_y_s = JW11_TP013(6).SIGNAL(t,2);
T2_x_s = JW11_TP013(6).SIGNAL(t,1);

T3_y_s = JW11_TP013(7).SIGNAL(t,2);
T3_x_s = JW11_TP013(7).SIGNAL(t,1);

T4_y_s = JW11_TP013(8).SIGNAL(t,2);
T4_x_s = JW11_TP013(8).SIGNAL(t,1);

x = [ T1_x_s; T2_x_s; T3_x_s; T4_x_s];
y = [ T1_y_s; T2_y_s; T3_y_s; T4_y_s];

figure
plot(x,y,'x')

tongue_shape_s = fit(x,y, 'cubicinterp')
hold
plot(tongue_shape_s, [x(4),x(1)],[y(4),y(1)],'xg')

%plot(tongue_shape_s,[fu_TD_u_m(1),fu_TT_u_m(1)],[fu_TD_u_m(2),fu_TT_u_m(2)],'xg')


