
% this is the timestamp of the /s/'s to plot
timestamp = [
    9399.6; %
    %F_08_before2_lbl_surd2_TD(1).OFFSET;
    11399.6;
    12999.6;
    13399.6;
    ]

%timestamp = timestamp - 50

% number of lines to plot
n = size(timestamp)

t = round(timestamp/1000*JW11_TP013(5).SRATE);

% x and y coordinates for each sensor
%T1
T1_y_s = JW11_TP013(5).SIGNAL(t,2);
T1_x_s = JW11_TP013(5).SIGNAL(t,1);

T2_y_s = JW11_TP013(6).SIGNAL(t,2);
T2_x_s = JW11_TP013(6).SIGNAL(t,1);

T3_y_s = JW11_TP013(7).SIGNAL(t,2);
T3_x_s = JW11_TP013(7).SIGNAL(t,1);

T4_y_s = JW11_TP013(8).SIGNAL(t,2);
T4_x_s = JW11_TP013(8).SIGNAL(t,1);

figure
for j = 1:n(1)
    x = [ T1_x_s(j); T2_x_s(j); T3_x_s(j); T4_x_s(j)];
    y = [ T1_y_s(j); T2_y_s(j); T3_y_s(j); T4_y_s(j)];
    tongue_shape_s = fit(x,y, 'poly1')
    plot(x,y,'x')
    plot(tongue_shape_s, [x(4),x(1)],[y(4),y(1)],'xg')
    if j == 1
        hold
    end
end



%plot(tongue_shape_s,[fu_TD_u_m(1),fu_TT_u_m(1)],[fu_TD_u_m(2),fu_TT_u_m(2)],'xg')


