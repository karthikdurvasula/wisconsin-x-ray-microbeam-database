This is the README for mvaverage.m. 

Please type 'help mvaverage' for info about parameters.



mvaverage requires two input text files, one to specify which files are being averaged over between 
what msec bounds (see included sample fInput.txt), and one which specifies which trajectories to 
average (see included tInput.txt). Any comments in the files must be in C++ format (comment lines 
start with //). 

The files must not contain any blank lines or the function will fail.


Here is a sample run:

 mvaverage('fInput.txt','tInput.txt','~/MATLAB/averager/','R','Y',1)



fInput.txt and tInput.txt are described above.

My test mview files are in ~/MATLAB/averager/. The data will be limited to the y signal and 
right-aligned. The population curves will be drawn along with the average, since the draw bit is 
set to 1. 



On the output graph, the average signal is always in white. 

To find specific values along the output curves, use MATLAB's 'data cursor' tool, found on the toolbar of the output figure.