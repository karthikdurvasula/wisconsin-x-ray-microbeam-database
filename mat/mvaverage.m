function x = mvaverage(fName,tfName,dataDir,justify,dim,disp)
%mvaverage  - create average over several mview signals
%
%	usage:  mvaverage(fName,tfName,dataDir,align,dim,disp)
%
% DATADIR is the path to relevant datafiles
% FNAME is the name of the input text file specifying data and msec bounds
% TFNAME is the name of the input text file specifying trajectories
% ALIGN specifies whether to right ('R') or left ('L') align the data
% DIM specifies whether to average X ('X') or Y ('Y') dimension
% DISP specifies whether to display just average (0) or population (1)
%

% cnk 6/06

%display the results in some nice way
%set the data sample rate
sr = 200;

trajs = textread(tfName,'%s','commentstyle','c++');
[files, starts, ends] = textread(fName, '%s %u %u','commentstyle','c++');

%convert input delims into samples
starts = floor(starts*sr/1000)+1;
ends = floor(ends*sr/1000)+1;
%find minimum length
lengths = ends-starts;
minlen = min(lengths);
%make x-scale in msecs
xscale = (0:minlen)*1000/sr;
%clip lengths according to R or L justify
if justify == 'R',
    starts = starts+lengths-minlen;
else,
    ends = ends-lengths+minlen;
end;

signals = [];
%create new figure per signal type?
for n = 1:length(trajs),
    for j = 1:length(files),
            data = load([dataDir char(files(j)) '.mat']);
            names = fieldnames(data);
            data = getfield(data,names{1});
            data = mdp_PosOnly2D(data);
    
            names = {data.NAME};
            for i = 2 : length(names),
                k = findstr(data(i).NAME,'_');
                data(i).NAME(k) = [];				% "_" not permitted in trajectory names
                data(i).NAME = upper(data(i).NAME);
                if isequal(data(i).NAME,char(trajs(n))),
                    data = data(i);
                    break;
                end;
            end;
            
            s = data.SIGNAL;
            
            if dim == 'X',
                signals(j,:)=s(starts(j):ends(j),1)';
            else,
                signals(j,:)=s(starts(j):ends(j),2)';
            end;        
    end;
    average = sum(signals)/length(files);
    subplot(length(trajs),1,n);
    if disp == 1,
        plot(xscale,signals);
        hold on;
    end;
    plot(xscale,average,'Color',[1 1 1]);
    xlim([0 xscale(end)]);
    set(gca,'Color','black');
    title(trajs(n));
end;
        
        
        
        
        
        
        
        
        
        
        


