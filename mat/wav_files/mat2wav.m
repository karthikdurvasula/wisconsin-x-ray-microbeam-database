function mat2wav(fl, nbits, idx) %MAT2WAV - save audio track from array-of-structs MAT variable in MS WAV format % 
% usage: mat2wav(fl, nbits, idx) % 
% given cellstr filelist FL defining a list of MAT files holding array-of-struct data 
% this procedure saves data(idx) as a MS WAV file with the same name % 
% optional NBITS defaults to 12 % optional IDX defaults to 1 % 
% note that NBITS == 12 (+/- 2048 range) is appropriate for NI 64 channel A/D data (EMMA), 
% while NBITS == 16 (+/- 32768 range) is appropriate for EMA audio sources 
% mkt 07/06 
if nargin < 1, eval('help mat2wav'); return; end; 

if nargin<2 || isempty(nbits), nbits = 12; 
end; 

if nargin<3 || isempty(idx), idx = 1; 
end; 

fl = {fl.name}; 

for k = 1 : length(fl), 
    if (length(fl{k}) > 4) & (fl{k}(end-3:end) == '.mat'), 
        try, data = load(fl{k},fl{k}(1:end-4)); 
            data = eval(['data.',fl{k}(1:end-4)]); 
        catch, error(sprintf('unable to load array-of-structs data from %s', fl{k})); 
        end; 
        s = data(idx).SIGNAL ./ 2^(nbits-1); 
        try, wavwrite(s,data(idx).SRATE,16,[fl{k},'.wav']); 
        catch, error(sprintf('unable to write %s.wav', fl{k})); 
        end; 
        fprintf('wrote %s.wav\n', fl{k}); 
    end; 
end;