function x = mfigure(mfile,traj,rangeMin,rangeMax,dim)
%mfigure  - plot mview trajectory over a specified range
%
%	usage:  mfigure(mfile,traj,rangeMin,rangeMax,dim)
%
% MFILE is the mview data file containing the trajectory to plot
% TRAJ is the name of the trajectory to plot
% RANGEMIN specifies the timestamp (in ms) begining the target interval
% RANGEMAX specifies the timestamp (in ms) ending the target interval
% DIM specifies the dimension (x=1,y=2) to be plotted
% JS May 16, 2009

%set the data sampling rate
sr = 200;

data = load(mfile); % the dataset
names = fieldnames(data);
data = getfield(data, names{1});
names = {data.NAME};
idx = strmatch(traj,names,'exact'); %index of specified trajectory

%populates array with data from the target trajectory and dimension
data = data(idx).SIGNAL(:,dim);

%translates specified time interval into nearest samples
sampMin = rangeMin*sr/1000; 
sampMax = rangeMax*sr/1000; 

%delimits data by specified timestamps in terms of sample
data = data(sampMin:sampMax);

%defines range for the plot
range = [sampMin:sampMax] %range in samples
msrange = range*1000/sr   %range in ms

%defines millisecond range
plot(msrange, data, 'LineWidth',2,'marker','x'); % plotting x-axis in ms



        
        
        
        
        
        
        
        
        
        


