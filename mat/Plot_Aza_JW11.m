clear all

rangeMin = 179000;
rangeMax = 192000;

sr = 145.4363;

data = load('JW11_TP016'); % the dataset
data = data.JW11_TP016;

%populates array with data from the target trajectory and dimension
TTy = data(5).SIGNAL(:,2);

%translates specified time interval into nearest samples
sampMin = rangeMin/sr; 
sampMax = rangeMax/sr; 
sampMin=round(sampMin)
sampMax=round(sampMax)
%delimits data by specified timestamps in terms of sample

TTyrange = TTy(sampMin:sampMax);

%defines range for the plot
range = [sampMin:sampMax]; %range in samples
msrange = range/sr;   %range in ms

%defines millisecond range
%subplot(2,1,2);
plot(msrange, TTyrange); % plotting x-axis in ms


